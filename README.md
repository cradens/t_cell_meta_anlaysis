This readme for the downloading, processing, and analyzing of T cell RNA-Sequencing datasets as referenced by https://doi.org/10.1101/727362.

## Download RNA-Seq .sra files

All 11 datasets are publically available for download.

Use the shell script download_sras.sh to download sras. Usage as follows:

`download_sras.sh <file with list of sras>.txt <# of sra files to simultaneously download> <file path to save download lines>`

<file with list of sras>.txt is a file where each row is a distinct SRR ID. To obtain a list of all the SRR IDs, see metainfo_combined.txt.

---

## Process each .sra file into a trimmed fastq

1. Convert sra to fastq with sratoolkit.2.9.2-centos_linux64/fastqdump

	`fastqdump <SRR ID>.sra --split-3 --gzip`

2. Trim fastq with TrimGalore-0.5.0/trim_galore

	`trim_galore --paired --stringency 5 --length 35 -q 20 <SRR_1.fq.gz> <SRR_2.fq.gz>`

---

## Align fastqs to hg38 for splicing analyses and donor identification

1. Align fastqs to hg38 genome with STAR (2.5.2a)

	`STAR --genomeDir <hg38 GENOME> --readFilesIn <SRR_1_trimmed.fq.gz> <SRR_2_trimmed.fq.gz> --runThreadN <# threads> --outSAMtype BAM Unsorted --outFileNamePrefix <SRR> --outSAMattributes All --alignSJoverhangMin 8 --readFilesCommand zcat --outSAMunmapped Within`

2. Sort bam with samtools-1.9/samtools

	`samtools sort -@ <# threads> -o <SRR_sorted.bam> <SRR.bam>`
	
3. Index bam with samtools-1.9/samtools

	`samtools sort -@ <# threads> -o <SRR_sorted.bam> <SRR.bam>`

The <hg38 GENOME> was created with the following:

`STAR --runMode genomeGenerate --genomeDir <hg38 GENOME> --genomeFastaFiles Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz --runThreadN 16 --sjdbGTFfile Homo_sapiens.GRCh38.94.chr.gtf --sjdbOverhang 99 --limitGenomeGenerateRAM 16000000000`


[Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz](ftp://ftp.ensembl.org/pub/release-94/fasta/homo_sapiens/dna/Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz)

[Homo_sapiens.GRCh38.94.chr.gtf.gz](ftp://ftp.ensembl.org/pub/release-94/gtf/homo_sapiens/Homo_sapiens.GRCh38.94.chr.gtf.gz)

---

## Identify and quantify local splicing variations with MAJIQ

1. Identify and quantify local splicing variations in all samples with MAJIQ (2.1)

	`majiq build --conf settings.txt --nprov <# threads> --simplify 0.01 -o <majiq_build_out_directory> Homo_sapiens.GRCh38.94.chr.gff3`

2. Quantify differences in splicing with MAJIQ (2.1)

	`majiq deltapsi -grp1 <majiq_build_out_directory>/<SRR_A>.majiq -grp2 <majiq_build_out_directory>/<SRR_B>.majiq --nproc <# threads> --names <SRR_A> <SRR_B> --output <deltapsi_output_directory>`

3. Output summary statistics and quantifications from MAJIQ deltapsi with VOILA (2.0)

	`voila tsv -f <output>.tsv --threshold 0.2 --show-all --nprob <# threads> <majiq_build_out_directory>/splicegraph.sql <deltapsi_output_directory>`


[Homo_sapiens.GRCh38.94.chr.gff3.gz](ftp://ftp.ensembl.org/pub/release-94/gff3/homo_sapiens/Homo_sapiens.GRCh38.94.chr.gff3.gz)

---

## Call SNPs in each bam, merge SNP-calls, then run identify by descent analysis

1. Call SNPs in each BAM with bcftools-1.9/bcftools

	`bcftools  mpileup -Ou -f Homo_sapiens.GRCh38.dna.primary_assembly.fa --threads <# threads> <SRR>.bam | bcftools call -mv -Ob --threads <# threads> --output <SRR>.bcf`

2. Index bcf file

	`bcftools index <SRR>.bcf`

3. Merge bcf files

	`bcftools merge --threads <# threads> --output-type z --output <merged name>.vcf.gz <SRR_A>.bcf <SRR_B>.bcf <etc>`
	
4. Filter merged vcf

	`bcftools view --min-af 0.05 --output-type z <merged name>.vcf.gz > <merged name>.maf.vcf.gz`
	
5. Normalize merged vcf, convert to bcf

	`bcftools norm -m-any <merged name>.maf.vcf.gz | bcftools norm -Ob --check-ref w -f Homo_sapiens.GRCh38.dna.primary_assembly.fa > <merged name>.maf.bcf`

6. Index bcf

	`bcftools index <merged name>.maf.bcf`
	
7. Recode into plink file (plink 1.90b6.7)
	
	`plink --bcf <merged name>.maf.bcf --const-fid 0 --allow-extra-chr 0 --recode --out <plink_prefix>`

8. Run identify by descent analysis

	`plink --file <merged_out_directory> --genome --out <plink_prefix>`
	
9. Plot identify by descent results

	`Rscript plot_ibd.R <plink_prefix>.genome`

---

## Align fastqs to hg38 for expression analyses

1. Align fastqs to hg38 genome and calculate transcript-supporting read counts with Salmon (0.13.1)

	`salmon quant -i <salmon_hg38_index> -l A -p <# threads> -1 <SRR_1_trimmed.fq.gz> -2 <SRR_2_trimmed.fq.gz> -o salmon_quant_<SRR>`

2. Merge Salmon outputs and collapse transcript-level counts to gene-level counts with tximport (1.12.0) in R (3.5.1)

	`txi <- tximport(<salmon_quant_fps>, type = "salmon"); asinh_tpm <- asinh(txi$abundance)`

<salmon_quant_fps> is a list of Salmon quant.sf filepaths.
	
The <salmon_hg38_index> was created with the following:

`salmon index -t <hg38_transcripts.fa> -i <salmon_hg38_index> --type quasi -k 31`

The <hg38_transcripts.fa> was created with gffread (0.9.12) using the following command:

`gffread -w <hg38_transcripts.fa> -g Homo_sapiens.GRCh38.dna.primary_assembly.fa Homo_sapiens.GRCh38.94.chr.gtf`

---

## Idenfity differentially expressed genes between samples in a given dataset

1. Import salmon quant files with tximport (1.12.0) in R (3.5.1)

	`txi <- tximport(<salmon_quant_fps>, type = "salmon")`
	
2. Use tximport object to create DESeq2 gene expression object

	`dds <- DESeq2::DESeqDataSetFromTximport(txi, colData = meta_info, design = ~Donor + author_condition_hours)`
	
3. Run DESeq algorithm

	`dds <- DESeq(dds)`
	
4. Perform logFC shrinkage

	`resLFC <- lfcShrink(dds, type = "apeglm", lfcThreshold = 1.5, svalue = TRUE)`
	
meta_info is a data.frame with rows as samples, and the following columns:

 - SampleID (SRR)
 - Donor (Donor ID)
 - author_condition_hours (character combonation of the author of the dataset, the T cell subtype, and hours cultured, if any)
 