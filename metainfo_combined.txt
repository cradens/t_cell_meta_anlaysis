SampleID	Source	Author	Condition	HoursCultured	Donor	DiseaseStatus	autantibodies	ISM_state	Sex	ReadLength	Strand	UniqReadsNum	UniqReadsPerc	SplitReads	Species	TechnicalReplicateIdentifier	SortedOrCultured
ERR2217059	CORD_BLOOD	Henriksson	CD4_NAIVE	0.0	hen1	Healthy				125	forward	20953825	92.36333333333334	14057858	Human	Henriksson.Human.umbilical cord blood.hen1.CD4_NAIVE.0	Sorted from cord blood
ERR2217065	CORD_BLOOD	Henriksson	CD4_NAIVE	0.0	hen2	Healthy				125	forward	23233283	92.40333333333335	14771059	Human	Henriksson.Human.umbilical cord blood.hen2.CD4_NAIVE.0	Sorted from cord blood
ERR2217071	CORD_BLOOD	Henriksson	CD4_NAIVE	0.0	hen3	Healthy				125	forward	20888848	92.84666666666665	13914049	Human	Henriksson.Human.umbilical cord blood.hen3.CD4_NAIVE.0	Sorted from cord blood
ERR2217221	CORD_BLOOD	Henriksson	CD4_TH0	72.0	hen1	Healthy				125	forward	24423703	93.12166666666667	20055933	Human	Henriksson.Human.umbilical cord blood.hen1.CD4_TH0.72	Sorted from cord blood and cultured
ERR2217227	CORD_BLOOD	Henriksson	CD4_TH0	72.0	hen2	Healthy				125	forward	26060538	92.86	21722128	Human	Henriksson.Human.umbilical cord blood.hen2.CD4_TH0.72	Sorted from cord blood and cultured
ERR2217233	CORD_BLOOD	Henriksson	CD4_TH0	72.0	hen3	Healthy				125	forward	23841972	94.005	18970175	Human	Henriksson.Human.umbilical cord blood.hen3.CD4_TH0.72	Sorted from cord blood and cultured
ERR2217383	CORD_BLOOD	Henriksson	CD4_TH2	72.0	hen1	Healthy				125	forward	24161783	93.26666666666667	19530650	Human	Henriksson.Human.umbilical cord blood.hen1.CD4_TH2.72	Sorted from cord blood and cultured
ERR2217389	CORD_BLOOD	Henriksson	CD4_TH2	72.0	hen2	Healthy				125	forward	23531923	93.10833333333333	18992719	Human	Henriksson.Human.umbilical cord blood.hen2.CD4_TH2.72	Sorted from cord blood and cultured
ERR2217395	CORD_BLOOD	Henriksson	CD4_TH2	72.0	hen3	Healthy				125	forward	24592294	93.64833333333333	20570585	Human	Henriksson.Human.umbilical cord blood.hen3.CD4_TH2.72	Sorted from cord blood and cultured
ERR2000445	ADULT_BLOOD	Simillion	CD4_TH1	0.0	sim01-02	Healthy				100	reverse	29310807	94.15	9993356	Human	Simillion.Human.blood.sim01-02.CD4_TH1.0	Sorted from blood and cultured
ERR2000446	ADULT_BLOOD	Simillion	CD4_TH1	0.0	sim01-17	Healthy				100	reverse	32740864	93.95	11359218	Human	Simillion.Human.blood.sim01-17.CD4_TH1.0	Sorted from blood and cultured
ERR2000447	ADULT_BLOOD	Simillion	CD4_TH1	0.0	sim01-19	Healthy				100	reverse	49217331	93.92	15128581	Human	Simillion.Human.blood.sim01-19.CD4_TH1.0	Sorted from blood and cultured
ERR2000449	ADULT_BLOOD	Simillion	CD4_TH17	0.0	sim17-12	Healthy				100	reverse	34370750	94.58	10330168	Human	Simillion.Human.blood.sim17-12.CD4_TH17.0	Sorted from blood and cultured
ERR2000450	ADULT_BLOOD	Simillion	CD4_TH17	0.0	sim17-17	Healthy				100	reverse	22012238	93.39	7602910	Human	Simillion.Human.blood.sim17-17.CD4_TH17.0	Sorted from blood and cultured
ERR2000451	ADULT_BLOOD	Simillion	CD4_TH17	0.0	sim17-20	Healthy				100	reverse	42547750	94.57	12589341	Human	Simillion.Human.blood.sim17-20.CD4_TH17.0	Sorted from blood and cultured
ERR2000452	ADULT_BLOOD	Simillion	CD4_TH2	0.0	sim02-03	Healthy				100	reverse	23518473	93.25	9068416	Human	Simillion.Human.blood.sim02-03.CD4_TH2.0	Sorted from blood and cultured
ERR2000453	ADULT_BLOOD	Simillion	CD4_TH2	0.0	sim02-06	Healthy				100	reverse	46068017	94.24	14671095	Human	Simillion.Human.blood.sim02-06.CD4_TH2.0	Sorted from blood and cultured
ERR2000454	ADULT_BLOOD	Simillion	CD4_TH2	0.0	sim02-08	Healthy				100	reverse	39540431	93.5	13086160	Human	Simillion.Human.blood.sim02-08.CD4_TH2.0	Sorted from blood and cultured
ERR2000448	ADULT_BLOOD	Simillion	CD4_TH1	0.0	simB	Healthy				100	reverse	27481801	94.37	8817532	Human	Simillion.Human.blood.simB.CD4_TH1.0	Sorted from blood and cultured
ERR2000455	ADULT_BLOOD	Simillion	CD4_TH2	0.0	simK	Healthy				100	reverse	41504100	94.13	14235087	Human	Simillion.Human.blood.simK.CD4_TH2.0	Sorted from blood and cultured
ERR2000456	ADULT_BLOOD	Simillion	CD4_TH2	0.0	simL	Healthy				100	reverse	26526862	93.79	10249853	Human	Simillion.Human.blood.simL.CD4_TH2.0	Sorted from blood and cultured
ERR2000457	ADULT_BLOOD	Simillion	CD4_TH2	0.0	simN	Healthy				100	reverse	25444682	94.2	10099199	Human	Simillion.Human.blood.simN.CD4_TH2.0	Sorted from blood and cultured
ERR431622	ADULT_BLOOD	Ranzani	CD4_TH1	0.0	ran1	Healthy			Unknown	101	None	9861423	92.03	3800522	Human	Ranzani.Human.blood.ran1.CD4_TH1.0	Sorted from blood
ERR431597	ADULT_BLOOD	Ranzani	CD4_TH17	0.0	ran2	Healthy			Unknown	101	None	8848777	92.4	3980573	Human	Ranzani.Human.blood.ran2.CD4_TH17.0	Sorted from blood
ERR431628	ADULT_BLOOD	Ranzani	CD4_TH2	0.0	ran3	Healthy			Unknown	101	None	11306589	93.37	5472801	Human	Ranzani.Human.blood.ran3.CD4_TH2.0	Sorted from blood
ERR431579	ADULT_BLOOD	Ranzani	CD4_TH2	0.0	ran4	Healthy			Unknown	101	None	12019206	93.59	5992150	Human	Ranzani.Human.blood.ran4.CD4_TH2.0	Sorted from blood
ERR431583	ADULT_BLOOD	Ranzani	CD4_TREG	0.0	ran5	Healthy			Unknown	101	None	13389412	93.03	5894978	Human	Ranzani.Human.blood.ran5.CD4_TREG.0	Sorted from blood
ERR431606	ADULT_BLOOD	Ranzani	CD4_TREG	0.0	ran6	Healthy			Unknown	101	None	11935426	92.92	5508194	Human	Ranzani.Human.blood.ran6.CD4_TREG.0	Sorted from blood
ERR431609	ADULT_BLOOD	Ranzani	CD4_TREG	0.0	ran7	Healthy			Unknown	101	None	14409136	92.4	6194391	Human	Ranzani.Human.blood.ran7.CD4_TREG.0	Sorted from blood
ERR431584	ADULT_BLOOD	Ranzani	CD4_TH1	0.0	ran8	Healthy			Unknown	101	None	10104431	91.4	4091739	Human	Ranzani.Human.blood.ran8.CD4_TH1.0	Sorted from blood
ERR431577	ADULT_BLOOD	Ranzani	CD4_TH1	0.0	ran9	Healthy			Unknown	101	None	13147443	91.77	5595764	Human	Ranzani.Human.blood.ran9.CD4_TH1.0	Sorted from blood
ERR431570	ADULT_BLOOD	Ranzani	CD4_TH1	0.0	ran10	Healthy			Unknown	101	None	9980646	91.98	4417076	Human	Ranzani.Human.blood.ran10.CD4_TH1.0	Sorted from blood
ERR431587	ADULT_BLOOD	Ranzani	CD4_TH1	0.0	ran11	Healthy			Unknown	101	None	9867032	92.45	4187687	Human	Ranzani.Human.blood.ran11.CD4_TH1.0	Sorted from blood
ERR431625	ADULT_BLOOD	Ranzani	CD4_TH17	0.0	ran12	Healthy			Unknown	101	None	9668413	91.78	3963319	Human	Ranzani.Human.blood.ran12.CD4_TH17.0	Sorted from blood
ERR431604	ADULT_BLOOD	Ranzani	CD4_TH17	0.0	ran13	Healthy			Unknown	101	None	8834205	91.8	3924740	Human	Ranzani.Human.blood.ran13.CD4_TH17.0	Sorted from blood
ERR431595	ADULT_BLOOD	Ranzani	CD4_TH17	0.0	ran14	Healthy			Unknown	101	None	7408124	90.14	3356712	Human	Ranzani.Human.blood.ran14.CD4_TH17.0	Sorted from blood
ERR431618	ADULT_BLOOD	Ranzani	CD4_TH17	0.0	ran15	Healthy			Unknown	101	None	9149856	91.49	3625472	Human	Ranzani.Human.blood.ran15.CD4_TH17.0	Sorted from blood
ERR431600	ADULT_BLOOD	Ranzani	CD4_TH2	0.0	ran16	Healthy			Unknown	101	None	10228827	92.54	5326101	Human	Ranzani.Human.blood.ran16.CD4_TH2.0	Sorted from blood
ERR431566	ADULT_BLOOD	Ranzani	CD4_TH2	0.0	ran17	Healthy			Unknown	101	None	9803573	92.81	4837458	Human	Ranzani.Human.blood.ran17.CD4_TH2.0	Sorted from blood
ERR431615	ADULT_BLOOD	Ranzani	CD4_TH2	0.0	ran18	Healthy			Unknown	101	None	11189649	93.03	5089779	Human	Ranzani.Human.blood.ran18.CD4_TH2.0	Sorted from blood
ERR431601	ADULT_BLOOD	Ranzani	CD4_TREG	0.0	ran19	Healthy			Unknown	101	None	11972124	92.15	5304711	Human	Ranzani.Human.blood.ran19.CD4_TREG.0	Sorted from blood
ERR431589	ADULT_BLOOD	Ranzani	CD4_TREG	0.0	ran20	Healthy			Unknown	101	None	11707613	92.24	6705917	Human	Ranzani.Human.blood.ran20.CD4_TREG.0	Sorted from blood
ERR431580	ADULT_BLOOD	Ranzani	CD4_NAIVE	0.0	ran21	Healthy			Unknown	101	None	4552400	94.0	2386947	Human	Ranzani.Human.blood.ran21.CD4_NAIVE.0	Sorted from blood
ERR431610	ADULT_BLOOD	Ranzani	CD4_NAIVE	0.0	ran22	Healthy			Unknown	101	None	12896897	94.43	7004402	Human	Ranzani.Human.blood.ran22.CD4_NAIVE.0	Sorted from blood
ERR431571	ADULT_BLOOD	Ranzani	CD4_NAIVE	0.0	ran23	Healthy			Unknown	101	None	17555923	94.12	9426123	Human	Ranzani.Human.blood.ran23.CD4_NAIVE.0	Sorted from blood
ERR431581	ADULT_BLOOD	Ranzani	CD4_NAIVE	0.0	ran60	Healthy			Unknown	101	None	35652171	89.71	16210583	Human	Ranzani.Human.blood.ran60.CD4_NAIVE.0	Sorted from blood
ERR431574	ADULT_BLOOD	Ranzani	CD4_NAIVE	0.0	ran61	Healthy			Unknown	101	None	35628547	89.67	16132565	Human	Ranzani.Human.blood.ran61.CD4_NAIVE.0	Sorted from blood
SRR7647679	ADULT_BLOOD	Calderon	CD4_NAIVE	0.0	cal01	Healthy			Unknown	0	none	52713705	94.27	39236703	Human	Calderon.Human.blood.cal01.CD4_NAIVE.0	Sorted from blood
SRR7647682	ADULT_BLOOD	Calderon	CD4_TREG	0.0	cal01	Healthy			Unknown	0	none	60674876	90.55	41619583	Human	Calderon.Human.blood.cal01.CD4_TREG.0	Sorted from blood
SRR7647684	ADULT_BLOOD	Calderon	CD4_TH1	0.0	cal01	Healthy			Unknown	0	none	44592721	90.68	29650950	Human	Calderon.Human.blood.cal01.CD4_TH1.0	Sorted from blood
SRR7647686	ADULT_BLOOD	Calderon	CD4_TH17	0.0	cal01	Healthy			Unknown	0	none	55986101	90.42	33869609	Human	Calderon.Human.blood.cal01.CD4_TH17.0	Sorted from blood
SRR7647688	ADULT_BLOOD	Calderon	CD4_TH2	0.0	cal01	Healthy			Unknown	0	none	35626154	89.43	22744735	Human	Calderon.Human.blood.cal01.CD4_TH2.0	Sorted from blood
SRR7647715	ADULT_BLOOD	Calderon	CD4_NAIVE	0.0	cal02	Healthy			Unknown	0	none	31355793	84.57	13467538	Human	Calderon.Human.blood.cal02.CD4_NAIVE.0	Sorted from blood
SRR7647717	ADULT_BLOOD	Calderon	CD4_TREG	0.0	cal02	Healthy			Unknown	0	none	42623364	80.73	23972196	Human	Calderon.Human.blood.cal02.CD4_TREG.0	Sorted from blood
SRR7647719	ADULT_BLOOD	Calderon	CD4_TH1	0.0	cal02	Healthy			Unknown	0	none	27042395	88.41	18302110	Human	Calderon.Human.blood.cal02.CD4_TH1.0	Sorted from blood
SRR7647721	ADULT_BLOOD	Calderon	CD4_TH17	0.0	cal02	Healthy			Unknown	0	none	27275949	86.37	18811315	Human	Calderon.Human.blood.cal02.CD4_TH17.0	Sorted from blood
SRR7647723	ADULT_BLOOD	Calderon	CD4_TH2	0.0	cal02	Healthy			Unknown	0	none	31396324	89.04	21217506	Human	Calderon.Human.blood.cal02.CD4_TH2.0	Sorted from blood
SRR7647753	ADULT_BLOOD	Calderon	CD4_NAIVE	0.0	cal03	Healthy			Unknown	0	none	11234914	88.2	5732045	Human	Calderon.Human.blood.cal03.CD4_NAIVE.0	Sorted from blood
SRR7647755	ADULT_BLOOD	Calderon	CD4_TREG	0.0	cal03	Healthy			Unknown	0	none	45486397	87.5	22334375	Human	Calderon.Human.blood.cal03.CD4_TREG.0	Sorted from blood
SRR7647757	ADULT_BLOOD	Calderon	CD4_TH1	0.0	cal03	Healthy			Unknown	0	none	8862475	89.91	4245354	Human	Calderon.Human.blood.cal03.CD4_TH1.0	Sorted from blood
SRR7647759	ADULT_BLOOD	Calderon	CD4_TH17	0.0	cal03	Healthy			Unknown	0	none	21012836	90.95	12321339	Human	Calderon.Human.blood.cal03.CD4_TH17.0	Sorted from blood
SRR7647761	ADULT_BLOOD	Calderon	CD4_TH2	0.0	cal03	Healthy			Unknown	0	none	27410155	85.98	11314491	Human	Calderon.Human.blood.cal03.CD4_TH2.0	Sorted from blood
SRR7647788	ADULT_BLOOD	Calderon	CD4_NAIVE	0.0	cal04	Healthy			Unknown	0	none	42055960	91.37	24435151	Human	Calderon.Human.blood.cal04.CD4_NAIVE.0	Sorted from blood
SRR7647792	ADULT_BLOOD	Calderon	CD4_TREG	0.0	cal04	Healthy			Unknown	0	none	44357157	89.75	19815874	Human	Calderon.Human.blood.cal04.CD4_TREG.0	Sorted from blood
SRR7647811	ADULT_BLOOD	Calderon	CD4_TH1	0.0	cal10	Healthy			Unknown	0	none	63742294	90.81	15907465	Human	Calderon.Human.blood.cal10.CD4_TH1.0	Sorted from blood
SRR7647812	ADULT_BLOOD	Calderon	CD4_TH17	0.0	cal10	Healthy			Unknown	0	none	26558267	88.58	6072447	Human	Calderon.Human.blood.cal10.CD4_TH17.0	Sorted from blood
SRR7647813	ADULT_BLOOD	Calderon	CD4_TH2	0.0	cal10	Healthy			Unknown	0	none	15698375	89.37	3632007	Human	Calderon.Human.blood.cal10.CD4_TH2.0	Sorted from blood
ND307-0hr	ADULT_BLOOD	Davia	CD4_NAIVE	0.0	ND307	Healthy			M	150	None	62897796	84.27	53563108	Human	Davia.Human.blood.ND307.CD4_NAIVE.0	Sorted from blood
ND307-48hr-CD3-CD28	ADULT_BLOOD	Davia	CD4_TH0	48.0	ND307	Healthy			M	150	None	67475000	92.42	72391912	Human	Davia.Human.blood.ND307.CD4_TH0.48	Sorted from blood and cultured
ND523-0hr	ADULT_BLOOD	Davia	CD4_NAIVE	0.0	ND523	Healthy			F	150	None	52986406	91.23	51510491	Human	Davia.Human.blood.ND523.CD4_NAIVE.0	Sorted from blood
ND523-48hr-CD3-CD28	ADULT_BLOOD	Davia	CD4_TH0	48.0	ND523	Healthy			F	150	None	64484246	92.21	70670012	Human	Davia.Human.blood.ND523.CD4_TH0.48	Sorted from blood and cultured
ND535-0hr	ADULT_BLOOD	Davia	CD4_NAIVE	0.0	ND535	Healthy			M	150	None	61885051	91.46	53809866	Human	Davia.Human.blood.ND535.CD4_NAIVE.0	Sorted from blood
ND535-48hr-CD3-CD28	ADULT_BLOOD	Davia	CD4_TH0	48.0	ND535	Healthy			M	150	None	78328832	92.49	79412710	Human	Davia.Human.blood.ND535.CD4_TH0.48	Sorted from blood and cultured
SRR1028227	CORD_BLOOD	Tuomela	CD4_NAIVE	0.0	tuo1	Healthy				75	None	25630940	92.63	8116015	Human	Tuomela.Human.umbilical cord blood.tuo1.CD4_NAIVE.0	Sorted from cord blood
SRR1028235	CORD_BLOOD	Tuomela	CD4_TH0	72.0	tuo1	Healthy				50	None	31709437	84.85	5337325	Human	Tuomela.Human.umbilical cord blood.tuo1.CD4_TH0.72	Sorted from cord blood and cultured
SRR1028244	CORD_BLOOD	Tuomela	CD4_TH17	72.0	tuo1	Healthy				50	None	36076448	84.9	6163576	Human	Tuomela.Human.umbilical cord blood.tuo1.CD4_TH17.72	Sorted from cord blood and cultured
SRR1028245	CORD_BLOOD	Tuomela	CD4_NAIVE	0.0	tuo2	Healthy				50	None	31340256	84.88	3680127	Human	Tuomela.Human.umbilical cord blood.tuo2.CD4_NAIVE.0	Sorted from cord blood
SRR1028254	CORD_BLOOD	Tuomela	CD4_TH0	72.0	tuo2	Healthy				50	None	28550773	83.39	4923963	Human	Tuomela.Human.umbilical cord blood.tuo2.CD4_TH0.72	Sorted from cord blood and cultured
SRR1028263	CORD_BLOOD	Tuomela	CD4_TH17	72.0	tuo2	Healthy				50	None	28617358	81.89	4606580	Human	Tuomela.Human.umbilical cord blood.tuo2.CD4_TH17.72	Sorted from cord blood and cultured
SRR1028264	CORD_BLOOD	Tuomela	CD4_NAIVE	0.0	tuo3	Healthy				50	None	33819430	84.47	3747545	Human	Tuomela.Human.umbilical cord blood.tuo3.CD4_NAIVE.0	Sorted from cord blood
SRR1028273	CORD_BLOOD	Tuomela	CD4_TH0	72.0	tuo3	Healthy				50	None	30333795	83.49	5016604	Human	Tuomela.Human.umbilical cord blood.tuo3.CD4_TH0.72	Sorted from cord blood and cultured
SRR1028282	CORD_BLOOD	Tuomela	CD4_TH17	72.0	tuo3	Healthy				50	None	21539806	82.97	3643818	Human	Tuomela.Human.umbilical cord blood.tuo3.CD4_TH17.72	Sorted from cord blood and cultured
SRR2141381	CORD_BLOOD	Kanduri	CD4_TH2	72.0	kan3	Healthy			Unknown	51	None	181611548	82.58	30188841	Human	Kanduri.Human.umbilical cord blood.kan3.CD4_TH2.72	Sorted from cord blood and cultured
SRR2141379	CORD_BLOOD	Kanduri	CD4_TH2	72.0	kan1	Healthy			Unknown	51	None	152053855	82.98	24824564	Human	Kanduri.Human.umbilical cord blood.kan1.CD4_TH2.72	Sorted from cord blood and cultured
SRR2141380	CORD_BLOOD	Kanduri	CD4_TH2	72.0	kan2	Healthy			Unknown	51	None	171737705	82.7	27748267	Human	Kanduri.Human.umbilical cord blood.kan2.CD4_TH2.72	Sorted from cord blood and cultured
SRR2141378	CORD_BLOOD	Kanduri	CD4_TH1	72.0	kan3	Healthy			Unknown	51	None	178543204	83.19	28617905	Human	Kanduri.Human.umbilical cord blood.kan3.CD4_TH1.72	Sorted from cord blood and cultured
SRR2141376	CORD_BLOOD	Kanduri	CD4_TH1	72.0	kan1	Healthy			Unknown	51	None	162005774	83.55	25679872	Human	Kanduri.Human.umbilical cord blood.kan1.CD4_TH1.72	Sorted from cord blood and cultured
SRR2141377	CORD_BLOOD	Kanduri	CD4_TH1	72.0	kan2	Healthy			Unknown	51	None	179877721	82.91	28841550	Human	Kanduri.Human.umbilical cord blood.kan2.CD4_TH1.72	Sorted from cord blood and cultured
SRR2141375	CORD_BLOOD	Kanduri	CD4_TH0	72.0	kan3	Healthy			Unknown	51	None	167699542	82.19	27593212	Human	Kanduri.Human.umbilical cord blood.kan3.CD4_TH0.72	Sorted from cord blood and cultured
SRR2141374	CORD_BLOOD	Kanduri	CD4_TH0	72.0	kan2	Healthy			Unknown	51	None	177529132	81.44	29497021	Human	Kanduri.Human.umbilical cord blood.kan2.CD4_TH0.72	Sorted from cord blood and cultured
SRR2141373	CORD_BLOOD	Kanduri	CD4_TH0	72.0	kan1	Healthy			Unknown	51	None	154173313	83.11	23751850	Human	Kanduri.Human.umbilical cord blood.kan1.CD4_TH0.72	Sorted from cord blood and cultured
SRR2141372	CORD_BLOOD	Kanduri	CD4_NAIVE	0.0	kan3	Healthy			Unknown	51	None	131340600	82.2	16858511	Human	Kanduri.Human.umbilical cord blood.kan3.CD4_NAIVE.0	Sorted from cord blood
SRR2141370	CORD_BLOOD	Kanduri	CD4_NAIVE	0.0	kan1	Healthy			Unknown	51	None	153576318	83.68	17473226	Human	Kanduri.Human.umbilical cord blood.kan1.CD4_NAIVE.0	Sorted from cord blood
SRR2141371	CORD_BLOOD	Kanduri	CD4_NAIVE	0.0	kan2	Healthy			Unknown	51	None	146190809	81.96	18622171	Human	Kanduri.Human.umbilical cord blood.kan2.CD4_NAIVE.0	Sorted from cord blood
SRR6368708	ADULT_BLOOD	Abadier	CD4_NAIVE	0.0	aba35	Healthy			F	50	reverse	38070675	77.41	5155831	Human	Abadier.Human.blood.aba35.CD4_NAIVE.0	Sorted from blood
SRR6368709	ADULT_BLOOD	Abadier	CD4_NAIVE	0.0	aba38	Healthy			F	50	reverse	29759422	73.83	4198817	Human	Abadier.Human.blood.aba38.CD4_NAIVE.0	Sorted from blood
SRR6368710	ADULT_BLOOD	Abadier	CD4_NAIVE	0.0	aba32	Healthy			M	50	reverse	35773186	78.98	5413861	Human	Abadier.Human.blood.aba32.CD4_NAIVE.0	Sorted from blood
SRR6368711	ADULT_BLOOD	Abadier	CD4_NAIVE	0.0	aba29	Healthy			F	50	reverse	37055522	76.49	5496931	Human	Abadier.Human.blood.aba29.CD4_NAIVE.0	Sorted from blood
SRR6368712	ADULT_BLOOD	Abadier	CD4_TH1	0.0	aba35	Healthy			F	50	reverse	50950704	82.37	7079031	Human	Abadier.Human.blood.aba35.CD4_TH1.0	Sorted from blood and cultured
SRR6368713	ADULT_BLOOD	Abadier	CD4_TH1	0.0	aba38	Healthy			F	50	reverse	36366773	81.43	5022650	Human	Abadier.Human.blood.aba38.CD4_TH1.0	Sorted from blood and cultured
SRR6368714	ADULT_BLOOD	Abadier	CD4_TH1	0.0	aba32	Healthy			M	50	reverse	40173965	81.85	5946112	Human	Abadier.Human.blood.aba32.CD4_TH1.0	Sorted from blood and cultured
SRR6368715	ADULT_BLOOD	Abadier	CD4_TH1	0.0	aba29	Healthy			F	50	reverse	54418470	83.61	7737336	Human	Abadier.Human.blood.aba29.CD4_TH1.0	Sorted from blood and cultured
SRR6667860	ADULT_BLOOD	Revu	CD4_TH0	120.0	rev15	Healthy			Unknown	75	reverse	77460445	90.93	23564688	Human	Revu.Human.blood.rev15.CD4_TH0.120	Sorted from blood and cultured
SRR6667864	ADULT_BLOOD	Revu	CD4_TH17	120.0	rev15	Healthy			Unknown	75	reverse	73617560	90.95500000000001	21557407	Human	Revu.Human.blood.rev15.CD4_TH17.120	Sorted from blood and cultured
SRR6667867	ADULT_BLOOD	Revu	CD4_TH0	120.0	rev16	Healthy			Unknown	75	reverse	76728159	91.225	21550560	Human	Revu.Human.blood.rev16.CD4_TH0.120	Sorted from blood and cultured
SRR6667870	ADULT_BLOOD	Revu	CD4_TH17	120.0	rev16	Healthy			Unknown	75	reverse	74354254	89.425	19890062	Human	Revu.Human.blood.rev16.CD4_TH17.120	Sorted from blood and cultured
SRR1615171	ADULT_BLOOD	Hertweck	CD4_NAIVE	0.0	her1	Healthy			Unknown	50	forward	14976463	67.01	2890971	Human	Hertweck.Human.blood.her1.CD4_NAIVE.0	Sorted from blood
SRR1615172	ADULT_BLOOD	Hertweck	CD4_TH1	324.0	her1	Healthy			Unknown	50	forward	13907509	65.04	3474995	Human	Hertweck.Human.blood.her1.CD4_TH1.324	Sorted from blood and cultured
SRR1615174	ADULT_BLOOD	Hertweck	CD4_TH2	324.0	her1	Healthy			Unknown	50	forward	17794185	65.85	4812741	Human	Hertweck.Human.blood.her1.CD4_TH2.324	Sorted from blood and cultured
SRR1615180	ADULT_BLOOD	Hertweck	CD4_NAIVE	0.0	her2	Healthy			Unknown	50	forward	22529665	67.21	4906174	Human	Hertweck.Human.blood.her2.CD4_NAIVE.0	Sorted from blood
SRR1615181	ADULT_BLOOD	Hertweck	CD4_TH1	324.0	her2	Healthy			Unknown	50	forward	28981787	64.83	8514871	Human	Hertweck.Human.blood.her2.CD4_TH1.324	Sorted from blood and cultured
SRR1615183	ADULT_BLOOD	Hertweck	CD4_TH2	324.0	her2	Healthy			Unknown	50	forward	26681409	64.84	8356233	Human	Hertweck.Human.blood.her2.CD4_TH2.324	Sorted from blood and cultured
SRR6298266	ADULT_BLOOD	Monaco	CD4_TREG	0.0	mon2	Healthy			M	50	none	16877606	89.83	3599318	Human	Monaco.Human.blood.mon2.CD4_TREG.0	Sorted from blood
SRR6298267	ADULT_BLOOD	Monaco	CD4_TH1	0.0	mon2	Healthy			M	50	none	15296873	86.6	3032690	Human	Monaco.Human.blood.mon2.CD4_TH1.0	Sorted from blood
SRR6298269	ADULT_BLOOD	Monaco	CD4_TH17	0.0	mon2	Healthy			M	50	none	19177794	88.92	4412294	Human	Monaco.Human.blood.mon2.CD4_TH17.0	Sorted from blood
SRR6298270	ADULT_BLOOD	Monaco	CD4_TH2	0.0	mon2	Healthy			M	50	none	16085014	87.92	3800629	Human	Monaco.Human.blood.mon2.CD4_TH2.0	Sorted from blood
SRR6298271	ADULT_BLOOD	Monaco	CD4_NAIVE	0.0	mon2	Healthy			M	50	none	12440290	88.27	2717587	Human	Monaco.Human.blood.mon2.CD4_NAIVE.0	Sorted from blood
SRR6298294	ADULT_BLOOD	Monaco	CD4_TREG	0.0	mon3	Healthy			M	50	none	18961327	90.05	4408641	Human	Monaco.Human.blood.mon3.CD4_TREG.0	Sorted from blood
SRR6298295	ADULT_BLOOD	Monaco	CD4_TH1	0.0	mon3	Healthy			M	50	none	21153387	88.49	4632418	Human	Monaco.Human.blood.mon3.CD4_TH1.0	Sorted from blood
SRR6298297	ADULT_BLOOD	Monaco	CD4_TH17	0.0	mon3	Healthy			M	50	none	16099504	90.12	3541926	Human	Monaco.Human.blood.mon3.CD4_TH17.0	Sorted from blood
SRR6298298	ADULT_BLOOD	Monaco	CD4_TH2	0.0	mon3	Healthy			M	50	none	15896532	82.68	3331694	Human	Monaco.Human.blood.mon3.CD4_TH2.0	Sorted from blood
SRR6298299	ADULT_BLOOD	Monaco	CD4_NAIVE	0.0	mon3	Healthy			M	50	none	18342576	85.01	3498486	Human	Monaco.Human.blood.mon3.CD4_NAIVE.0	Sorted from blood
SRR6298323	ADULT_BLOOD	Monaco	CD4_TREG	0.0	mon4	Healthy			F	50	none	15097077	88.11	3142777	Human	Monaco.Human.blood.mon4.CD4_TREG.0	Sorted from blood
SRR6298324	ADULT_BLOOD	Monaco	CD4_TH1	0.0	mon4	Healthy			F	50	none	17416085	87.47	3845730	Human	Monaco.Human.blood.mon4.CD4_TH1.0	Sorted from blood
SRR6298326	ADULT_BLOOD	Monaco	CD4_TH17	0.0	mon4	Healthy			F	50	none	17063981	89.14	3585123	Human	Monaco.Human.blood.mon4.CD4_TH17.0	Sorted from blood
SRR6298327	ADULT_BLOOD	Monaco	CD4_TH2	0.0	mon4	Healthy			F	50	none	16116523	87.3	3551976	Human	Monaco.Human.blood.mon4.CD4_TH2.0	Sorted from blood
SRR6298328	ADULT_BLOOD	Monaco	CD4_NAIVE	0.0	mon4	Healthy			F	50	none	16239157	86.25	3365113	Human	Monaco.Human.blood.mon4.CD4_NAIVE.0	Sorted from blood
SRR6298358	ADULT_BLOOD	Monaco	CD4_TREG	0.0	mon1	Healthy			F	50	none	14681755	84.77	3077503	Human	Monaco.Human.blood.mon1.CD4_TREG.0	Sorted from blood
SRR3185800	ADULT_BLOOD	Locci	CD4_TH1	72.0	loc40	Healthy			Unknown	50	reverse	7193705	80.54	1076442	Human	Locci.Human.blood.loc40.CD4_TH1.72	Sorted from blood and cultured
SRR3185798	ADULT_BLOOD	Locci	CD4_TH0	72.0	loc40	Healthy			Unknown	50	reverse	5098531	80.01	750065	Human	Locci.Human.blood.loc40.CD4_TH0.72	Sorted from blood and cultured
SRR3185795	ADULT_BLOOD	Locci	CD4_TH1	72.0	loc38	Healthy			Unknown	50	reverse	5174271	81.3	769906	Human	Locci.Human.blood.loc38.CD4_TH1.72	Sorted from blood and cultured
SRR3185794	ADULT_BLOOD	Locci	CD4_TH0	72.0	loc38	Healthy			Unknown	50	reverse	4082367	82.19	595446	Human	Locci.Human.blood.loc38.CD4_TH0.72	Sorted from blood and cultured
SRR3185789	ADULT_BLOOD	Locci	CD4_TH1	72.0	loc11	Healthy			Unknown	50	reverse	7136107	81.55	1194467	Human	Locci.Human.blood.loc11.CD4_TH1.72	Sorted from blood and cultured
SRR3185788	ADULT_BLOOD	Locci	CD4_TH0	72.0	loc11	Healthy			Unknown	50	reverse	3906378	81.89	605764	Human	Locci.Human.blood.loc11.CD4_TH0.72	Sorted from blood and cultured
SRR3185783	ADULT_BLOOD	Locci	CD4_TH1	72.0	loc05	Healthy			Unknown	50	reverse	3109378	82.58	478990	Human	Locci.Human.blood.loc05.CD4_TH1.72	Sorted from blood and cultured
SRR3185782	ADULT_BLOOD	Locci	CD4_TH0	72.0	loc05	Healthy			Unknown	50	reverse	2980099	82.46	445643	Human	Locci.Human.blood.loc05.CD4_TH0.72	Sorted from blood and cultured